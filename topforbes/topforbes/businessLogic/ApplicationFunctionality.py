from sqlalchemy import create_engine,Column,Integer,String
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
import pymysql
from database_credentials import *
from RichPersonDatabaseModel import *


class ApplicationFunctionality:
    def __init__(self):
        self.connection_string = f'{database_type}://{username}:{password}@localhost:{port}/{database_name}'
        self.engine = create_engine(self.connection_string)
        self.Session = sessionmaker(bind=self.engine)
        self.session = self.Session()

    def getTopTenYoungestRichPerson(self):
        richPersons = self.session.query(RichPerson).order_by(RichPerson.age).limit(10)
        for richperson in richPersons:
            print(f'{richperson.name} are {richperson.age} ani')
    def getAmericanRichPerson(self):
        americanRichPersonsNumber = self.session.query(RichPerson).filter(RichPerson.country=="United States").count()
        anotherContryRichPersonNumber = 200 - americanRichPersonsNumber
        print(f'Exista {americanRichPersonsNumber} persoane care au cetatenie americana.')
        print(f'Exista {anotherContryRichPersonNumber} care au alta cetatenie.')

    def getTopTenPhilanthropyScore(self):
        richPersons = self.session.query(RichPerson).order_by(RichPerson.philanthropyScore.desc()).limit(10)
        for richperson in richPersons:
            print(f'{richperson.name} are {richperson.philanthropyScore} scor filantropic.')

def main():
    app = ApplicationFunctionality()
    app.getTopTenYoungestRichPerson()
    print("----------------------\n")
    app.getAmericanRichPerson()
    print("-------------------\n")
    app.getTopTenPhilanthropyScore()


main()
