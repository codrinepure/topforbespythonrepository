from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine,Column,Integer,String
from sqlalchemy.orm import sessionmaker
Base = declarative_base()


class RichPerson(Base):
    __tablename__= 'richperson'

    id=Column(Integer,primary_key=True)
    name=Column(String(100))
    age=Column(Integer)
    country=Column(String(50))
    philanthropyScore = Column(Integer)
