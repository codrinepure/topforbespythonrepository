import scrapy
import re
from scrapy.http import Request
import json

from sqlalchemy import create_engine,Column,Integer,String
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
import pymysql

Base = declarative_base()

username="root"
password="root"
database_name="topforbesdb"
database_type="mysql+pymysql"
port="3305"



URL_SUPPORT = 'https://www.forbes.com/billionaires'
BASE_URL = 'https://www.forbes.com/'
searched_regular_expression = "https://[\\.\\-\\/a-zA-Z0-9_]*\\.json|https://[\\.\\-\\/a-zA-Z0-9_]*\\.js"

class RichPerson(Base):
    __tablename__= 'richperson'

    id=Column(Integer,primary_key=True)
    name=Column(String(100))
    age=Column(Integer)
    country=Column(String(50))
    philanthropyScore = Column(Integer)



class ForbesSpider(scrapy.Spider):
    name='forbes'
    start_urls=[
        URL_SUPPORT
    ]
    def __init__(self):
       self.crawled = False 
       self.engine = None
       self.session = None


    def parse(self,response):
        if self.crawled is False:
            self.crawled = True
            link_htmlElements_list =response.css('link::attr(href)').extract()
            print(len(link_htmlElements_list))
            for link in link_htmlElements_list:
                link = link.strip()
                match_jsFile = link.endswith('.js')
             
                if match_jsFile:
                    next_url = BASE_URL + link
                    with open("urls.txt","a") as f:
                        f.write(next_url)
                        f.write("\n-----------------\n")
                        yield response.follow(next_url,callback=self.parse)

        else:
            found_regular_expression_list = re.findall(searched_regular_expression,response.text)
            for link in found_regular_expression_list:
                link=link.strip()
                match_jsonFile = link.endswith('.json')
                if match_jsonFile:
                    yield Request(link,callback=self.handle_jsonFile)
                else:
                   yield response.follow(link,callback=self.parse)

    def checkJsonFileStatisticsBeforeStoreData(self,namesCount,ageCount,countryCount,philanthropyScoreCount):
        if namesCount < 200 or ageCount < 200 or countryCount < 200 or philanthropyScoreCount < 200:
            return False
        return True

    def createConnectionTopForbesDB(self,username,password,database_name,database_type,port):
        connection_string = f'{database_type}://{username}:{password}@localhost:{port}/{database_name}'
        self.engine = create_engine(connection_string)
        Session = sessionmaker(bind=self.engine)
        self.session = Session()
    
    def addNewRichPerson(self,richPersonModel):
        name = richPersonModel["name"]
        age = richPersonModel["age"]
        country = richPersonModel["country"]
        philanthropyScore = richPersonModel["philanthropyScore"]

        newRichPerson = RichPerson(name=name,age=age,country=country,philanthropyScore=philanthropyScore)

        self.session.add(newRichPerson)
        self.session.commit()

    def createRichPersonModel(self,name,age,country,philanthropyScore):
        newRichPersonModel = dict()
        newRichPersonModel["name"]=name
        newRichPersonModel["age"]=age
        newRichPersonModel["country"]=country
        newRichPersonModel["philanthropyScore"]=philanthropyScore
        return newRichPersonModel

    def prepareJSONResultData(self,jsonResultDictionary):
        index=0
        self.createConnectionTopForbesDB(username,password,database_name,database_type,port)
        print("connection created")


        while index<200:

            name=jsonResultDictionary['personList']['personsLists'][index]['person']['name']
            if "age" in jsonResultDictionary['personList']['personsLists'][index]:
                age=jsonResultDictionary['personList']['personsLists'][index]['age']
            else:
                age=100
            country=jsonResultDictionary['personList']['personsLists'][index]['country']
            if "philanthropyScore" in jsonResultDictionary['personList']['personsLists'][index]:
               philanthropyScore=jsonResultDictionary['personList']['personsLists'][index]['philanthropyScore']
            else:
                philanthropyScore=-1

            newRichPersonModel = self.createRichPersonModel(name,age,country,philanthropyScore)
            self.addNewRichPerson(newRichPersonModel)

            print(newRichPersonModel) 
            # with open("json_results.json","a") as results_jsonOutfile:
            #     json.dump(newRichPersonModel,results_jsonOutfile)
            index+=1
            print(index)

    def handle_jsonFile(self,response):
        namesCount = response.text.count('name')
        ageCount = response.text.count('age')
        countryCount = response.text.count("country")
        philanthropyScoreCount = response.text.count("philanthropyScore")

        if self.checkJsonFileStatisticsBeforeStoreData(namesCount,ageCount,countryCount,philanthropyScoreCount):
            jsonResultDictionary = json.loads(response.body_as_unicode())
            self.prepareJSONResultData(jsonResultDictionary)


       
      
            