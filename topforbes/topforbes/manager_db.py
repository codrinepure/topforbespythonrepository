from sqlalchemy import create_engine,Column,Integer,String
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
import pymysql
from credentials import *

Base = declarative_base()


class DBManager:
    def __init__(self):
        self.engine = None
        self.session = None
    def createTopForbesDatabase(self,username,password,database_name,database_type,port):
        connection_string = f'{database_type}://{username}:{password}@localhost:{port}/{database_name}'
        self.engine = create_engine(connection_string)
        Session = sessionmaker(bind=self.engine)
        self.session = Session()
    def getEngine(self):
        return self.engine
    def getSession(self):
        return self.session
    def createRichPersonTable(self):
        Base.metadata.create_all(self.engine)
    def addNewRichPerson(self,richPersonModel):
        name = richPersonModel["name"]
        age = richPersonModel["age"]
        country = richPersonModel["country"]
        philanthropyScore = richPersonModel["philanthropyScore"]

        newRichPerson = RichPerson(name=name,age=age,country=country,philanthropyScore=philanthropyScore)

        self.session.add(newRichPerson)
        self.session.commit()

class RichPerson(Base):
    __tablename__= 'richperson'

    id=Column(Integer,primary_key=True)
    name=Column(String(100))
    age=Column(Integer)
    country=Column(String(50))
    philanthropyScore = Column(Integer)


def main():
    managerDB = DBManager()
    print("am creat managerdb")
    managerDB.createTopForbesDatabase(username,password,database_name,database_type,port)
    # managerDB.createRichPersonTable()
    newRichPersonModel = dict()
    newRichPersonModel["name"]="andrei andrei"
    newRichPersonModel["age"]=21
    newRichPersonModel["country"]="romania"
    newRichPersonModel["philanthropyScore"]=5
    managerDB.addNewRichPerson(newRichPersonModel)

main()